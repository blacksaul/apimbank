var express = require('express'),
    bodyparser = require('body-parser'),
    jwt = require('jsonwebtoken'),
    app = express(),
    port = process.env.PORT || 3000;


const config = {
	llave : "miclaveultrasecreta123*"
};

var path = require('path');
const bcrypt = require('bcrypt');
//var bodyparser=require('body-parser');

/////////////////JSON WEB TOKEN ////////////////////////////////
app.set('llave', config.llave);
app.use(bodyparser.urlencoded({ extended: true }));


app.use(bodyparser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With, Content-Type, Accept,*");

  next();
})//cross origin, se le indica para peticiones inseguras deje pasar API Rest


var requestjson = require('request-json');
var urlMLabRaiz = "https://api.mlab.com/api/1/databases/srodriguez/collections/";
var urlBanxico = "https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43787,SF43784/datos/oportuno/";
var urlApiMarketGPS = "https://apis.bbvabancomer.com/locations_sbx/v1/atms";
var ApiMarketToken = "jwt eyJ6aXAiOiJERUYiLCJlbmMiOiJBMTI4R0NNIiwiYWxnIjoiUlNBLU9BRVAifQ.hfa_4TPZ47bYRQe2btiojTVSs-kAlrKaBsFr4QyZfcz7sLVZeKX0vMCMiXYOvtAZy5gqNdY518XJh9Fpw1_7PVx4PcX8C9ivHWAIPAA7m6g02LeM4G2zGWlMXQEcSI3T-XNPBMP169VJk1gzgj9wfjrthlf0-RGhnM7XVsobUgD9DgpdehCa0Imy-DD5rHqK54tpmH1JQJwrEtJACK_cI5c8BdWsuRhwd1r4jtQUY1h1vYlaaERel2NkxVtE4uEGH4ttYWG0RcuHAAG6kwhJsmtzN9CS7FQr_-WbQDJjK3Pz58b88gyY9qOiKGqjTc2KxYGb1TIEiQtMP8VIwJkUsQ._ULGQrIg7wH3tlJ_.NePvuf8ZUtulwUhIOEt0OP7y2XjcvFwTchmvIuvMDKdbWl21aAbX8vSk_t-ZGTX3DrIimhsi5yHSXc439q3JhljswSzJhYT4vUv9hyMNf7Psrk03Q0QDN8D6mUcCvA-CVNkLqlxDCX5Sc6ocX4EjpTVOIna-wxbcLN51diP2TnZnoqxwHDx6fALGZ-0TfQaVEZknEtznXsZUJw-vnQAMW-RcORGMYQ.DoMEuU26MBTKfFIVyopK8g";

var apiKey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var tokenBanxico = "?token=889ce2f49e533ae1b38b1fcfc1317b2e1f3dd11bd22b76194ca713f4dd466afb";
var collectionFeatures = "MBANK_FEATURES";
var collectionUsers = "MBANK_USERS";
var collectionMovements = "MBANK_MOVEMENTS";
var getLogin;

app.listen(port);

console.log('RESTful API server started on: ' + port);

app.get('/', function(req, res) {
    res.json({ message: 'recurso de entrada' });
});

// 6
const rutasProtegidas = express.Router();
rutasProtegidas.use((req, res, next) => {
    const token = req.headers['access-token'];
    //console.log("obtenido: " + token);
    if (token) {
      jwt.verify(token, app.get('llave'), (err, decoded) => {
        if (err) {
          return res.json({ mensaje: 'Token inválida' });
          console.log("token ivalida")
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      console.log("token no proveida")
      res.send({
          mensaje: 'Token no proveída.'
      });
    }
 });


app.get('/v1/getBanxico',  function(req, res){
  var doingBanxico = requestjson.createClient(urlBanxico + tokenBanxico);
  doingBanxico.get('', function(err, resM, body){
    if(!err){
      res.send(body);
    }else{
      console.log(err);
    }
  })
})

//Metodo para validar si existe o no el usuario
app.post('/v1/validateUser', function(req, res){
  var email = req.body.email;
  var query = '&q={"email":"' +email+ '"}';
  var doingValidate = requestjson.createClient(urlMLabRaiz + collectionUsers  + apiKey + query);
  doingValidate.get('', function(err, restM, body) {
    if(!err){
      if(body.length == 1){ //Login fue OK
        res.status(200).send('El usuario existe');
      }else{
        res.status(204).send('El usuario no existe');
      }
    }else{
      console.log(err);
    }
  })
})

//Metodo para crear un usuario nuevo
app.post('/v1/createUser', function(req, res){
  var bcrypStr;

  let hash = bcrypt.hashSync(req.body.password, 10);
  req.body.password = hash;
  var doUser = requestjson.createClient(urlMLabRaiz + collectionUsers + apiKey);
  doUser.post('', req.body, function(err, resM, body){
    if(!err){
      res.send(body);
    }else{
      console.log(err);
    }
  })
})

//Metodo para guardar las caracteristicas de los clientes
app.post('/v1/createFeatures', function(req, res){
  var doFeature = requestjson.createClient(urlMLabRaiz + collectionFeatures + apiKey);
  doFeature.post('', req.body, function(err, resM, body){
    if(!err){
      res.send(body);
    }else{
      console.log(err);
    }
  })
})


//Metodo para solicitar un acceso
app.post('/v1/login', (req, resapi) =>{
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"' +email+ '"}';

  getLogin = requestjson.createClient(urlMLabRaiz + collectionUsers + apiKey + "&" + query);

  getLogin.get('', function(errs, restM, body) {
    if(!errs){
      if(body.length == 1){ //Login fue OK
        if(bcrypt.compareSync(password, body[0].password)) {
          const payload = {
      			check:  true
      		};
      		const token = jwt.sign(payload, app.get('llave'), {
      			expiresIn: 300
      		});
      		resapi.json({
      			mensaje: 'Autenticación correcta',
      			token: token
      		});
          //console.log("El token es" + token);
          //resapi.status(200).send('Usuario loggueado');
        }else{
          resapi.status(201).send('Datos erroneos, verifiquelos de nuevo');
        }
      }else{
        resapi.status(204).send('No existe el usuario');
      }
    }else{
      console.log(errs);
    }
  })
})


//Metodo para obtener las caracteristicas de los clientes
app.post('/v1/getFeatures', rutasProtegidas, (req, res) => {
  var email = req.body.email;
  var query = '&q={"email":"' +email+ '"}';

  var getFeat = requestjson.createClient(urlMLabRaiz + collectionFeatures + apiKey + query);

  getFeat.get('', function(err, restM, body) {
    if(!err){
      res.send(body);
    }else{
      console.log(err);
    }
  })
})

//Metodo para agregar movimientos de dinero
app.post('/v1/addMovement',  rutasProtegidas, (req, res) => {
  var doAddMoney= requestjson.createClient(urlMLabRaiz + collectionMovements + apiKey);
  doAddMoney.post('', req.body, function(err, resM, body){
    if(!err){
      res.send(body);
    }else{
      console.log(err);
    }
  })
})

//Metodo para sumar o restar dinero
app.put('/v1/globalMoney',  rutasProtegidas, (req, res) => {
  var email = req.body.email;
  var valueImport = req.body.import;
  var typemovement = req.body.typemov;
  var query = '&q={"email":"' +email+ '"}';
  var totalImport = 0;
  var getFeat = requestjson.createClient(urlMLabRaiz + collectionFeatures + apiKey + query);
  var flagExecute = false;
  getFeat.get('', function(err, restM, body) {
    if(!err){
      var getGlobalMoney = body[0].contract.account_money;
      if(typemovement == "P"){
        totalImport =  parseInt(getGlobalMoney) + parseInt(valueImport);
        flagExecute = true;
      }else{
        if ((parseInt(getGlobalMoney) - parseInt(valueImport)) >= 0){
          totalImport = parseInt(getGlobalMoney) - parseInt(valueImport);
          flagExecute = true;
        }
        else{
          res.status(202);
          res.send('{"message":"El dinero es insuficiente."}');
        }
      }
      if (flagExecute == true){
        var doGlobalMoney = requestjson.createClient(urlMLabRaiz + collectionFeatures + apiKey + query);
        var putBody = '{"$set":{"contract.account_money": "' + totalImport + '"}}'
        req.body = JSON.parse(putBody);
        var doGlobalMoney = requestjson.createClient(urlMLabRaiz + collectionFeatures + apiKey + query);
        doGlobalMoney.put('', req.body, function(err, resM, body){
          if(!err){
            res.send('{"message":"Éxito: El dinero ha sido abonado."}');
          }else{
            console.log(err);
          }
        })
      }
    }else{
      console.log(err);
    }
  })
})


//Metodo para consultar API MARKET
app.get('/v1/getApiMarket',  rutasProtegidas, (req, res) => {
  var doingApiMarket = requestjson.createClient(urlApiMarketGPS);
  doingApiMarket.headers['Authorization'] = ApiMarketToken;
  doingApiMarket.get('', function(err, resM, body){
    if(!err){
      res.send(body);
    }else{
      console.log(err);
    }
  })
})

/*
app.post('/v1/getMovements',  rutasProtegidas, (req, res) => {
  var type_mov = req.body.transactionType;
  var query = '&s={"date_movement": -1, "time_movement": -1}&q={"transaction_type":"' +type_mov+ '"}';
  var doingMov = requestjson.createClient(urlMLabRaiz + collectionMovements + apiKey + query);
  doingMov.get('', function(err, resM, body){
    if(!err){
      res.send(body);
    }else{
      console.log(err);
    }
  })
})
*/
app.post('/v2/getMovements',  rutasProtegidas, (req, res) => {
  var type_mov = req.body.transactionType;
  var skip = req.body.skip;
  var limit = req.body.limit;
  var email = req.body.email;
  var query = '&sk=' + skip + '&l=' + limit + '&s={"date_movement": -1, "time_movement": -1}&q={"transaction_type":"' +type_mov+ '", "email_client":"' +email+ '"}';
  var doingMov = requestjson.createClient(urlMLabRaiz + collectionMovements + apiKey + query);
  doingMov.get('', function(err, resM, body){
    if(!err){
      res.send(body);
    }else{
      console.log(err);
    }
  })
})

app.post('/v1/countsMovements',  rutasProtegidas, (req, res) => {
  var type_mov = req.body.transactionType;
  var email = req.body.email;
  var query = '&c=true&s={"date_movement": -1, "time_movement": -1}&q={"transaction_type":"' +type_mov+ '", "email_client":"' +email+ '"}';
  var doingMov = requestjson.createClient(urlMLabRaiz + collectionMovements + apiKey + query);
  doingMov.get('', function(err, resM, body){
    if(!err){
      res.send(body.toString());
    }else{
      console.log(err);
    }
  })
})


app.post('/v1/graphMovements',  rutasProtegidas, (req, res) => {
  var email = req.body.email;
  var query = '&q={"transaction_type":"R", "email_client":"' + email+ '"}';
  //res.header("Content-Type","application/json");
  var doingGraph = requestjson.createClient(urlMLabRaiz + collectionMovements + apiKey + query);
  doingGraph.get('', function(err, resM, body){
    if(!err){
      var rent = 0;
      var internet = 0;
      var rec_saldo = 0;
      var entertainment = 0;
      var beauty = 0;
      var maintenance_home = 0;
      var automotive = 0;

      var globalBuy = 0;
      for (i = 0; i < body.length; i++) {
        if (body[i].segment == "Renta"){
          rent += parseInt(body[i].import);
        }else if (body[i].segment == "Internet"){
          internet += parseInt(body[i].import);
        }else if (body[i].segment == "Recarga de saldo"){
          rec_saldo += parseInt(body[i].import);
        }else if (body[i].segment == "Entretenimiento"){
          entertainment += parseInt(body[i].import);
        }else if (body[i].segment == "Belleza"){
          beauty += parseInt(body[i].import);
        }else if (body[i].segment == "Mantenimiento de Hogar"){
          maintenance_home += parseInt(body[i].import);
        }else if (body[i].segment == "Automotriz"){
          automotive += parseInt(body[i].import);
        }
        globalBuy += parseInt(body[i].import);
      }

      /*
      console.log(rent * 100/globalBuy);
      console.log(internet * 100/globalBuy);
      console.log(rec_saldo * 100/globalBuy);
      console.log(entertainment * 100/globalBuy);
      console.log(beauty * 100/globalBuy);
      console.log(maintenance_home * 100/globalBuy);
      console.log(automotive * 100/globalBuy);
      */

      var createSegment = {
        "rent": rent.toFixed(0),
        "internet": internet.toFixed(0),
        "rec_saldo": rec_saldo.toFixed(0),
        "entertainment": entertainment.toFixed(0),
        "beauty": beauty.toFixed(0),
        "maintenance_home": maintenance_home.toFixed(0),
        "automotive": automotive.toFixed(0)
      };
      var bodySegment = JSON.stringify(createSegment);
      //console.log(bodySegment);
      res.status(200).send(bodySegment);
    }else{
      console.log(err);
    }
  })
})
